<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        $request->validate([
            "judul"=>"required|unique:pertanyaan",
            "isi"=>"required"
        ]);
        DB::table('pertanyaan')->insert([
            "judul"=>$request["judul"],
            "isi"=>$request["isi"]
        ]);       
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Disimpan!');
    }

    public function index(){
        $posts = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('posts'));
    }

    public function show($id){
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.detail', compact('post'));
    }

    public function edit($id){
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('post'));
    }

    public function update($id, Request $request){
        $request->validate([
            "judul"=>"required|unique:pertanyaan",
            "isi"=>"required"
        ]);
        $queryUpdate = DB::table('pertanyaan')->where('id', $id)->update([
            "judul"=>$request["judul"],
            "isi"=>$request["isi"]
        ]);       
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Dirubah!');
    }

    public function destroy($id){
        $queryDelete = DB::table('pertanyaan')->where('id', $id)->delete();       
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Dihapus!');
    }
}
