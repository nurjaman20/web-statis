<html>
    <head>
        <h2>Buat Account Baru!</h2>
    </head>
    <body>
        <h4>Sign Up Form</h4>
        <form action="/welcome" method="POST">
        @csrf
            <label>
                First Name:
            </label>
            <br><br>
            <input type="text" name="first_name">
            <br><br>
            <label>
                Last Name:
            </label>
            <br><br>
            <input type="text" name="last_name">
            <br><br>
            <label>
                Gender:
            </label>
            <br><br>
            <input type="radio" name="gender" value="0">Male
            <br>
            <input type="radio" name="gender" value="1">Female
            <br>
            <input type="radio" name="gender" value="2">Other
            <br><br>
            <label>
                Nationality:
            </label>
            <br><br>
            <select>
                <option>
                    Indonesia
                </option>
                <option>
                    India
                </option>
                <option>
                    Islandia
                </option>
            </select>
            <br><br>
            <label>
                Language Spoken:
            </label>
            <br><br>
            <input type="checkbox" name="language" value="0">Bahasa Indonesia
            <br>
            <input type="checkbox" name="language" value="1">English
            <br>
            <input type="checkbox" name="language" value="2">Other
            <br><br>
            <label>
                Bio:
            </label>
            <br><br>
            <textarea cols="25" rows="10"></textarea>
            <br><br>
            <input type="submit" value="Sign Up">
        </form>
    </body>
</html>