@extends('partial.master')

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Tabel Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif
        <a class="btn btn-primary mb-2" href="/pertanyaan/create">Create Pertanyaan</a>
      <table class="table table-bordered">
        <thead>                  
          <tr>
            <th style="width: 10px">No</th>
            <th>Judul</th>
            <th>Isi</th>
            <th style="width: 40px">Actions</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($posts as $key=>$item)
              <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->judul}}</td>
                <td>{{$item->isi}}</td>
                <td style="display: flex;">
                    <a href="/pertanyaan/{{$item->id}}" class="btn btn-info btn-sm">view</a>
                    <a href="/pertanyaan/{{$item->id}}/edit" class="btn btn-warning btn-sm">edit</a>
                    <form action="/pertanyaan/{{$item->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger">
                    </form>
                </td>
              </tr>
          @empty
              <tr>
                  <td colspan="4" align="center">Tidak Ada Pertanyaan</td>
              </tr>  
          @endforelse
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection