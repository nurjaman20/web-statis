@extends('partial.master')

@section('content')
<ul class="list-group">
    <li class="list-group-item d-flex justify-content-between align-items-center">
        {{$post->judul}}
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
        {{$post->isi}}
    </li>
</ul>
@endsection